# PingLogger

This simple JavaFX app can be used to monitor your internet connection. It pings the ip address '1.1.1.1' every second and thn draw two graphics showing the percentage of failed and successfuls pinng and the time took by the ping, for the last minute's pings.

![Screenshot of the application](https://gitlab.com/Azn9/pinglogger/-/raw/master/.gitlab/image.png)