package sample;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

public class Main extends Application {

    private int failedPing  = 1;
    private int successPing = 1;
    private int totalPing   = 2;

    private List<TimerTask> timerTaskList = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Ping Logger v2");

        Scene scene;
        primaryStage.setScene(scene = new Scene(root, 1080, 720));
        primaryStage.show();

        PieChart pieChart = (PieChart) scene.lookup("#pie");
        ObservableList<Data> pieChartData = FXCollections.observableArrayList(new PieChart.Data("Échoués", failedPing), new PieChart.Data("Réussis", successPing));
        pieChart.setData(pieChartData);

        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();

        LineChart<String, Double> lineChart = (LineChart<String, Double>) scene.lookup("#ping");
        lineChart.setTitle("Ping RTT");

        XYChart.Series<String, Double> series = new XYChart.Series<>();

        lineChart.getData().add(series);

        InetAddress inetAddress = InetAddress.getByName("1.1.1.1");

        TimerTask t1;
        timerTaskList.add(t1 = new TimerTask() {
            @Override
            public void run() {
                long start = new GregorianCalendar().getTimeInMillis();

                try {
                    double duration = 1000;
                    if (inetAddress.isReachable(1000)) {
                        successPing++;
                        long finish = new GregorianCalendar().getTimeInMillis();

                        duration = (double) (finish - start);
                    } else {
                        failedPing++;
                    }

                    if (series.getData().size() >= 50)
                        series.getData().remove(0);

                    double finalDuration = duration;
                    Platform.runLater(() -> series.getData().add(new XYChart.Data<>(String.valueOf(totalPing - 2), finalDuration)));
                    totalPing++;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        new Timer().scheduleAtFixedRate(t1, 0L, 500L);


        TimerTask t2;
        timerTaskList.add(t2 = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    pieChart.getData().get(0).setName("Échoués (" + (failedPing * 100 / totalPing) + "%)");
                    pieChart.getData().get(1).setName("Réussis (" + (successPing * 100 / totalPing) + "%)");
                    pieChart.getData().get(0).setPieValue(failedPing);
                    pieChart.getData().get(1).setPieValue(successPing);
                });
            }
        });

        new Timer().scheduleAtFixedRate(t2, 10L, 1000L);
    }

    /*
        x   total
            100
     */

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        for (TimerTask timerTask : timerTaskList)
            timerTask.cancel();

        System.exit(0);
    }
}
